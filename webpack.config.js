const path = require('path'), fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// 0) Console: 
// A grade sacada dessa rotina. É que ela não mexe na estrutura do código em sí,
// Ela gerencia as saídas de console.log, vai ajudar no acompanhamento dos builds. 
const
    consoleSuccess = '\x1b[32m%s\x1b[1m',
    consoleTitle = '\x1b[33m%s\x1b[1m';



// 2.a) Qual é a ideia aqui: automatizar a componentização no webpack.
// Para que o desenvolvedor front-end apenas crie uma pasta e os arquivos '.html' e '.stories.js'

// 2.b) Seria possível aceitar arquivos com todos os modelos de nomes e formatos. Seria o ideal, mas pra impormos um padrão 
// e pra facilitar a procura por possiveis erros no futuro optei por padronizar os nomes de pastas e arquivos. Seria um
// ótimo ponto para execução de testes.

function checkPath(startPath, filter, callback) {

    if (!fs.existsSync(startPath)) {
        console.log("Erro de leitura ", startPath);
        return;
    }

    var files = fs.readdirSync(startPath);
    for (var i = 0; i < files.length; i++) {
        var filename = path.join(startPath, files[i]);
        var stat = fs.lstatSync(filename);

        if (stat.isDirectory()) {
            checkPath(filename, filter, callback); //Pastas e similares
        }
        else if (filter.test(filename)) {
            callback(filename);
        }
    };
};



// 1.c) Nesse ponto eu tentei de muitas formas fazer uma verificação que não permitisse duplicata de diretórios.
// Mas, como ainda não consegui isso sempre que um novo build acontecer o primeiro passo será
// Localizar e limpar o conteúdo do ./styles.scss
// Isso garante que o primeiro arquivo a ser importado ao styles.scss sempre será o globals.scss
// E isso injeta toda a camada de folhas de estilo automáticamente nos domínios do webpack
fs.writeFile('./src/styles.scss', '', function () { console.log('Limpando cache do sistema') })



// 3.a) Botando a busca de arquivos pra trabalhar: Procurando por estruturas .html
// Aqui nosso script vai fazer uma busca dentro de ./src/components e vai buscar o
// uri de cada componente que termine em html. toda vez que um novo componente for
// encontrado, ele será adicionado ao array htmlComponents. 

var htmlComponents = [];

console.log(consoleTitle, 'Carregando todos os índices')

const indexCheck = checkPath('./src/components/', /\.html$/, function (filename) {
    htmlName = filename.replace(/^.*[\\\/]/, '')

    // 3.b) IMPORTANTE! O NODE NÃO VAI REPORTAR SE EXISTIREM ARQUIVOS ONONIMOS. 
    // Precisamos criar uma rotina que compare os arquivos envolvidos e o mais recente prevaleça.
    // Ou duplicar o arquivo com um texto "copy" ao final do arquivo mais recente.

    // 3.c) Sempre que um componente for encontrado ele será adicionado ao array htmlComponents.
    // já como um objeto HtmlWebpackPlugin. Respeitando o padrão do array no script original do webpack.
    // isso vai garantir que não se forme um gargalo no momento da execução do webpack. 

    htmlComponents.push(new HtmlWebpackPlugin({
        filename: htmlName,
        template: './' + filename
    }))
});

console.log(consoleSuccess, 'Índices carregados com sucesso.')


// 4.a) Nova busca por arquivos: enfileirando arquivos SCSS e preparando a estrutura do SASS.
// O Sass foi um dos primeiros problemas quando tentamos trabalhar com o storybook. Se configurarmos
// o SASS de forma que atenda as dependências do webpack ele não será renderizado dentro do storybook.
// E se configurarmos o SASS de forma que atenda as dependências do storybook, ele fica de fora da lista
// de processamentos do webpack. 

// 4.b) Então nós vamos preparar um novo array, dessa vez chamado de sassComponents[]. Faremos uma varredura
// dentro do diretório ./src/* E vamos enfileirar no array todos os uri's relacionados aos arquivos .scss

var sassComponents = [];

console.log(consoleTitle, 'Carregando todos os estilos')

const stylesCheck = checkPath('./src/components/', /\.(sa|sc|c)ss$/, function (filename) {
    sassComponents.push('@import "./' + filename + '";')
})

// 4.c) Aqui uma intervenção simples, pego a lista de array e transformo em um bloco de código
// pronto para popular nosso (./styles.scss). Esse é o único arquivo que o webpack vai enfileirar no build.

sassComponents = sassComponents.join("\n")

// Respeitar o tempo de execução foi fundamental para resolver nossos problemas com variáveis e importação
// do bootstrap. Nosso array já enfileirou todos os estilos de forma correta e está pronto para o build.N
// Mas nosso webpack ainda não sabe que não aceitamos rotas duplicadas, elas quebram nosso build...
// Por isso no tópico 1.c) resolvemos apagar o arquivo styles.scss. Isso evita conflitos no momento do build.

// Não é o cenário ideal! em um projeto com 300 componentes por exemplo, o build executaria 1.200 novas verificações
// O ideal é pular rotas que sejam iguais no momento da varredura. E adicionar ao build apenas os novos componentes.

// 4. d) Vamos adicionar nosso array dentro do arquivo ./src/styles.scss
fs.appendFile('./src/styles.scss', sassComponents, function (err) {
    if (err) throw err;
    console.log(consoleSuccess, 'Arquivo styles.scss atualizado.')
});

console.log(consoleSuccess, 'Todos os estilos carregados.')
console.log(consoleSuccess, 'Tudo pronto! iniciando o webpack...')


//1.b) Preparando a lista dos plugins.
pluginsArray = [
    ...htmlComponents
];

module.exports = {
    entry: ['./src/styles.scss'],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(jpe?g$|\.gif$|\.png$|\.PNG$|\.svg$|\.woff?$|\.ttf$|\.eot)$/i,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    publicPath: '/assets/',
                    outputPath: './src/components/'
                }
            }
        ]
    },
    // 1.a) pluginsArray. Tratei a listagem dos plugins em um array alimentado por uma função 
    // que busca por arquivos dentro da pasta ./ src / components
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'stylesheets.css'
        }),
        ...pluginsArray
    ]
}